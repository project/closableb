
This module provides quick way to user to hide a closable-block by configuration.
It's useful if you are going to have announcement blocks.

To make a block closable,

  - go to block management page (/admin/build/block),
  - click on configure link on which block you want.
  - On block configuration form, under *User specific visibility settings*
    fieldset, select *Show this block by default, but let individual users hide
    it.*
  - Save block configuration.
  - *Dismiss* link will be appended to your block.

To override the strings, please read Drupal core ./sites/default/default.settings.php
