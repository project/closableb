<?php

/**
 * Callback to dismiss a block.
 */
function closableb_callback_dismiss($module, $delta) {
  closableb_toggle_user_block($status = FALSE, $module, $delta);

  $message = t(
    'Hide the <strong>@block_subject</strong> block. (!undo_link)',
    array(
      '@block_subject' => closableb_get_block_title($module, $delta),
      '!undo_link' => closableb_get_undo_link($module, $delta),
    )
  );

  drupal_set_message($message);

  drupal_goto('<front>');
}

/**
 * Callback to dismiss a block.
 */
function closableb_callback_undo($module, $delta) {
  closableb_toggle_user_block($status = TRUE, $module, $delta);
  drupal_goto('<front>');
}

/**
 * Enable/Disable a block for a specific user.
 *
 * @param  boolean $status
 * @param  string  $module
 * @param  string  $delta
 * @param  stdClass $account
 */
function closableb_toggle_user_block($status, $module, $delta, $account = NULL) {
  if (!$account) {
    $account = $GLOBALS['user'];
  }

  $block = isset($account->block) ? $account->block : array();
  $block[$module][$delta] = $status;
  user_save($account, array('block' => $block));
}

/**
 * Helper function to get subject of the block.
 *
 * @param  string $module
 * @param  string $delta
 * @return string
 */
function closableb_get_block_title($module, $delta) {
  global $theme;

  if (empty($theme)) {
    init_theme();
  }

  $title = db_select('block', 'b')
              ->fields('b', array('title'))
              ->condition('module', $module)
              ->condition('delta', $delta)
              ->condition(db_or()
                ->condition('theme', $theme)
                ->condition('theme', ''))
              ->execute()->fetchColumn();

  // Quite stupid, we have to re-generate the block array,
  // but Drupal 6 does not provide API to get block title
  if (empty($title)) {
    $function = "{$module}_block";
    $block = $function('view', $delta);
    $title = $block['subject'];
  }

  return $title;
}

/**
 * Get undo link.
 *
 * @param  string $module
 * @param  string $delta
 * @return string
 * @see closableb_callback_dismiss()
 */
function closableb_get_undo_link($module, $delta) {
  $text = t('Undo');
  $url = "closableb-undo/{$module}/{$delta}";
  $class = 'closableb-undo';

  if (module_exists('ctools')) {
    ctools_add_js('ajax-responder');
    $class .= ' ctools-use-ajax';
  }

  return l($text, $url, array(
    'query' => drupal_get_destination(),
    'attributes' => array('class' => $class)
  ));
}

/**
 * Get dismiss link.
 *
 * @param  string $module
 * @param  string $delta
 * @return string
 * @see closableb_callback_dismiss()
 */
function closableb_get_dismiss_link($module, $delta) {
  $text = t('Hide');
  $url = "closableb-dismiss/{$module}/{$delta}";
  $class = 'closableb-dismiss';

  if (module_exists('ctools')) {
    ctools_add_js('ajax-responder');
    $class .= ' ctools-use-ajax';
  }

  return sprintf(
    '<div class="closableb-dismiss-wrapper">%s</div>',
    l($text, $url, array(
      'query' => drupal_get_destination(),
      'attributes' => array('class' => $class)
    ))
  );
}
